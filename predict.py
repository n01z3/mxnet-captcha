import mxnet as mx
import cv2
import numpy as np


def gen_sample(fpath, width=int(150/1.5), height=int(60/1.5)):
    img = cv2.imread(fpath)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (width, height))
    img = np.multiply(img, 1 / 255.0)
    img = img.transpose(2, 0, 1)
    return img


def get_model():
    model = mx.model.FeedForward.load('./models/new_chkpt', 4, ctx=mx.cpu(), numpy_batch_size=1)
    return model


def predict():
    model = get_model()

    for num in ['12774', '55124', '93057', '77894', '93057', '17530']:
        fn = 'examples/%s.png' % num
        img = gen_sample(fn)
        sampels = mx.nd.array([img])

        preds = model.predict(sampels)

        preds = np.array(preds)[:, 0, :]

        print np.argmax(preds, axis=1), num


if __name__ == '__main__':
    predict()
