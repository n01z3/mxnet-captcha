from PIL import Image
from PIL import ImageFilter, ImageFont
from PIL.ImageDraw import Draw
from PIL.ImageFont import truetype
import random
from matplotlib import pyplot as plt
import os


fonts_var = ['fonts/DejaVuSansMono.ttf', 'fonts/DejaVuSansMono-Bold.ttf', 'fonts/DejaVuSansMono-Oblique.ttf']
sizes = [30, 35, 40]

font = []
for f in fonts_var:
    for s in sizes:
        font.append(ImageFont.truetype(f, s))
_width, _height = 150, 60

table  =  []
# font = [ImageFont.truetype("futura1.ttf", 30), ImageFont.truetype("futura1.ttf", 35), ImageFont.truetype("futura1.ttf", 40)]

for  i  in  range( 256 ):
    table.append( i * 4 )

colors = [(154, 25, 179), (25, 127, 179), (50, 179, 25)]


def create_noise_curve(image, color):
    w, h = image.size
    x1 = random.randint(0, int(w / 5))
    x2 = random.randint(w - int(w / 5), w)
    y1 = random.randint(h / 5, h - int(h / 5))
    y2 = random.randint(y1, h - int(h / 5))
    points = [(x1, y1), (x2, y2)]
    end = random.randint(160, 200)
    start = random.randint(0, 20)
    # Draw(image).arc(points, start, end, fill=color)
    dr = Draw(image)
    dr.line(points, fill="black", width=4)
    for point in points:
        dr.ellipse((point[0] - 2, point[1] - 2, point[0] + 2, point[1] + 2), fill="black")
    # im.save("polygon.png")

    return image

def create_captcha_image(chars, color, background):
    """Create the CAPTCHA image itself.
    :param chars: text to be generated.
    :param color: color of the text.
    :param background: color of the background.
    The color should be a tuple of 3 numbers, such as (0, 255, 255).
    """
    image = Image.new('RGB', (_width, _height), background)
    draw = Draw(image)

    def _draw_character(c):
        # font = None #random.choice(truefonts)
        chs_font = font[random.randint(0,8)]
        w, h = draw.textsize(c, font=chs_font)

        dx = -1#random.randint(0, 4)
        dy = 0#random.randint(0, 6)
        im = Image.new('RGBA', (w + dx, h + dy))
        Draw(im).text((dx, dy), c, font=chs_font, fill=colors[random.randint(0,2)])


        return im

    images = []
    for c in chars:
        images.append(_draw_character(c))

    text_width = sum([im.size[0] for im in images])

    width = max(text_width, _width)
    image = image.resize((width, _height))

    average = int(text_width / len(chars))
    rand = int(0.25 * average)
    offset = int(average * 0.5)

    for im in images:
        w, h = im.size
        mask = im.convert('L').point(table)
        image.paste(im, (offset, int((_height - h) / 2)), mask)
        offset = offset + w + random.randint(-rand, 0)

    return image

def random_color(start, end, opacity=None):
    red = random.randint(start, end)
    green = random.randint(start, end)
    blue = random.randint(start, end)
    if opacity is None:
        return (red, green, blue)
    return (red, green, blue, opacity)

def generate_image(chars):

    background = (239,245,248)#
    color = random_color(0, 200, random.randint(220, 255))
    im = create_captcha_image(chars, color, background)

    create_noise_curve(im, (0, 0, 0))
    create_noise_curve(im, (0, 0, 0))

    # im = im.filter(ImageFilter.SMOOTH)
    return im


def make_imgs():
    for i in ['12774', '55124', '93057', '77894', '93057', '17530']:
        num = str(i)
        num = (5 - len(num)) * '0' + num

        img = generate_image(num)
        img.save('./imgs/' + num + '.jpg', "JPEG")

if __name__ == '__main__':
    im = generate_image('12345')
    plt.imshow(im)
    plt.show()
    # make_imgs()